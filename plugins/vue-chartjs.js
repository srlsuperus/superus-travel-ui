import Vue from 'vue'
import { Line } from 'vue-chartjs'

Vue.component('line-chart', {
    extends: Line,
    props: ['lineData', 'options'],
    mounted () {
        this.renderChart(this.lineData, this.options || {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                    type: 'time'
                }]
            }
        })
    }
})
