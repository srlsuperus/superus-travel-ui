module.exports = {

    /*
    ** Headers of the page
    */
    head: {
        title: 'superus-travel-ui',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: 'SUPERUS Travel Search Engine'}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
        ]
    },

    mode: 'spa',

    /*
    ** Customize the progress-bar color
    */
    loading: {color: '#3B8070'},

    /*
    ** Build configuration
    */
    build: {
        /*
        ** Run ESLINT on save
        */
        extend (config, ctx) {
            if (ctx.dev && ctx.isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })
            }
        },

        vendor: ['vue-moment', 'vue-i18n', 'vue-chartjs', 'chart.js']
    },

    router: {
        middleware: 'i18n'
    },

    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/font-awesome',
        '@nuxtjs/bootstrap-vue',
        ['@nuxtjs/pwa', {icon: false}],
        '@nuxtjs/webpackmonitor',
        '@nuxtjs/sentry',
        ['@nuxtjs/google-analytics', {id: 'UA-109730248-1'}],
        '@nuxtjs/sitemap'
    ],

    plugins: [
        '~/plugins/vue-moment',
        '~/plugins/i18n.js',
        '~plugins/vue-chartjs.js'
    ],

    generate: {
        routes: ['/', '/ro', '/en']
    },

    axios: {
        baseURL: process.env.NODE_ENV !== 'development' ? 'https://api.travel.superus.md' : 'http://localhost:8080',
        browserBaseURL: process.env.NODE_ENV !== 'development' ? 'https://api.travel.superus.md' : 'http://localhost:8080'
    },

    sitemap: {
        path: '/sitemap.xml',
        hostname: 'https://travel.superus.md',
        cacheTime: 1000 * 60 * 15,
        generate: true,
        routes () {
            const axios = require('axios')
            let airports = ['KIV', 'IAS', 'OTP', 'IEV', 'SCV', 'LED']
            return axios.get('https://api.travel.superus.md/airports').then((res) => {
                let lists = []
                res.data.data.airports.filter(airport => airports.indexOf(airport.iata) >= 0).forEach((el) => {
                    el.connections.forEach((connection) => {
                        lists.push('/discover/' + el.iata + '/' + connection)
                    })
                })
                return lists
            })
        }
    },

    sentry: {dsn: 'https://bd8fa21c9fc140ac8ff7b45c01fca0f6:ab4537bb8894446da36a1d45552e4a4c@sentry.io/238059'}
}
